#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <libgnome/gnome-desktop-item.h>

#include "uf-view.h"
#include "uf-gui.h"

extern GtkWidget *uf_app;

GnomeUIInfo toolbar[] = {
	GNOMEUIINFO_ITEM_STOCK(N_("Previous"),
			       N_("Go to the previous strip"),
			       on_previous_click, GTK_STOCK_GO_BACK),

	GNOMEUIINFO_ITEM_STOCK(N_("Next"), N_("Go to the next strip"),
			       on_next_click, GTK_STOCK_GO_FORWARD),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_ITEM_STOCK(N_("Fetch"), N_("Show selected strip"),
			       on_fetch_button_clicked,
			       GTK_STOCK_REFRESH),

	GNOMEUIINFO_ITEM_STOCK(N_("Today"), N_("Show today's strip"),
			       on_today_button_clicked,
			       GTK_STOCK_JUMP_TO),

	GNOMEUIINFO_ITEM_STOCK(N_("Preferences"), N_("Preferences"),
			       on_prefs_button_clicked,
			       GTK_STOCK_PREFERENCES),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_ITEM_STOCK(N_("Exit"),
			       N_("Quit the User Friendly Viewer"),
			       quit_app, GTK_STOCK_QUIT),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_ITEM_STOCK(N_("About"),
			       N_("About the User Friendly Viewer"),
			       about_app, GNOME_STOCK_ABOUT),

	GNOMEUIINFO_END,

};

static char *
get_label_from_desktop_file (char *path)
{
	GnomeDesktopItem *ditem;
	const char *name, *date;
	char *retval;

	ditem = gnome_desktop_item_new_from_file (path, 0, NULL);
	if (ditem == NULL)
		return NULL;

	/* check type */
	{
		int type;

		type = gnome_desktop_item_get_entry_type(ditem);
		if (type != GNOME_DESKTOP_ITEM_TYPE_LINK)
		{
			gnome_desktop_item_unref (ditem);
			return NULL;
		}
	}

	name = gnome_desktop_item_get_string (ditem, GNOME_DESKTOP_ITEM_NAME);

	/* Check if the date exists, if not, it's probably not one of ours */
	date = gnome_desktop_item_get_string (ditem, "X-UFVIEW-StartDate");
	if (date == NULL)
	{
		gnome_desktop_item_unref (ditem);
		return NULL;
	}

	retval = g_strdup (name);
	gnome_desktop_item_unref (ditem);

	return retval;
}

GtkWidget *create_prefs(char *selected_comic)
{
	GtkWidget *prefs;
	GtkWidget *align, *menu, *drop_down, *item;
	GDir *dir;
	const char *filename;
	char *dirpath;
	int i = 0, selection = -1;

	/* the property window */
	prefs = gtk_dialog_new_with_buttons (_("Select a comic"),
			GTK_WINDOW(uf_app),
			0,		/* flags */
			GTK_STOCK_CLOSE, GTK_RESPONSE_ACCEPT,
			NULL);
	gtk_container_set_border_width
		(GTK_CONTAINER (GTK_DIALOG (prefs)->vbox), 6);

	align = gtk_alignment_new(0.5, 0.5, 0, 0);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG (prefs)->vbox), align);
	gtk_container_set_border_width(GTK_CONTAINER(align), 6);
	gtk_widget_show(align);

	menu = gtk_menu_new();
	gtk_widget_show(menu);
	/* creating the menu items */
	dirpath = gnome_program_locate_file (NULL,
			GNOME_FILE_DOMAIN_APP_DATADIR,
			"uf-view/", FALSE, NULL);
	dir = g_dir_open (dirpath, 0, NULL);
	if (dir == NULL)
	{
		show_error (_("UF-View isn't installed properly"));
		goto bail;
	}
	filename = g_dir_read_name (dir);
	while (filename != NULL)
	{
		char *filepath, *label;

		filepath = g_build_filename (dirpath, filename, NULL);
		label = get_label_from_desktop_file (filepath);

		if (label != NULL)
		{
			item = gtk_menu_item_new_with_label (label);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
			g_signal_connect (G_OBJECT (item), "activate",
					G_CALLBACK (on_comic_select_changed),
					NULL);
			g_object_set_data (G_OBJECT (item), "filepath",
					(gpointer) filepath);
			gtk_widget_show (item);

			/* selection */
			if (selected_comic != NULL &&
					!strcmp (selected_comic, filepath))
				selection = i;
		} else {
			g_free (filepath);
		}

		filename = g_dir_read_name (dir);
		i++;
	}
	g_dir_close (dir);
	g_free (dirpath);

bail:

	drop_down = gtk_option_menu_new();
	gtk_widget_show(drop_down);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (drop_down), menu);
	gtk_container_add(GTK_CONTAINER(align), drop_down);

	if (selection != -1)
		gtk_option_menu_set_history(GTK_OPTION_MENU(drop_down),
				selection);

	g_signal_connect (G_OBJECT (prefs), "response",
			 G_CALLBACK (gtk_widget_hide), (gpointer) prefs);
	g_signal_connect (G_OBJECT (prefs), "delete-event",
			G_CALLBACK (gtk_widget_hide), (gpointer) prefs);

	return prefs;
}

GtkWidget *create_uf_app()
{
	GtkWidget *uf_app;
	GtkWidget *vbox1;
	GtkWidget *calendar;
	GtkWidget *uf_pixmap;
	GtkWidget *appbar;
	uf_data *foo = malloc(sizeof(uf_data));
	char *pmf;

	uf_app = gnome_app_new("uf-view", _("uf-view"));
	gtk_window_set_title(GTK_WINDOW(uf_app),
			     _("User Friendly Viewer"));

	vbox1 = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox1);
	gnome_app_set_contents(GNOME_APP(uf_app), vbox1);

	calendar = gtk_calendar_new();
	gtk_widget_show(calendar);
	gtk_box_pack_start(GTK_BOX(vbox1), calendar, TRUE, TRUE, 0);
	gtk_calendar_display_options(GTK_CALENDAR(calendar),
				     GTK_CALENDAR_SHOW_HEADING |
				     GTK_CALENDAR_SHOW_DAY_NAMES |
				     GTK_CALENDAR_WEEK_START_MONDAY);

	pmf = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_PIXMAP,
			"uf-view/uf-logo.png", FALSE, NULL);
	uf_pixmap = gtk_image_new_from_file(pmf);
	g_free(pmf);

	if (uf_pixmap == NULL)
		g_error(_("Couldn't create pixmap"));
	gtk_widget_show(uf_pixmap);
	gtk_box_pack_start(GTK_BOX(vbox1), uf_pixmap, TRUE, TRUE, 0);


	appbar = gnome_appbar_new(FALSE, TRUE, FALSE);
	gnome_app_set_statusbar(GNOME_APP(uf_app), GTK_WIDGET(appbar));
	gnome_app_create_toolbar(GNOME_APP(uf_app), toolbar);

	foo->calendar = GTK_CALENDAR(calendar);
	foo->pixmap = GTK_IMAGE(uf_pixmap);
	foo->appbar = GNOME_APPBAR(appbar);

	gnome_appbar_push(foo->appbar,
			  _
			  ("Select a date and click \"Fetch\" to view a cartoon."));

	g_signal_connect(G_OBJECT(calendar),
			 "day_selected_double_click",
			 G_CALLBACK (on_fetch_button_clicked), foo);
	g_signal_connect(G_OBJECT(uf_app), "destroy", G_CALLBACK (quit_app), foo);

	uf = foo;

	gnome_config_push_prefix("/uf-view/General/");
	uf->selected_comic = gnome_config_get_string("selected_comic=");
	gnome_config_pop_prefix();

	return uf_app;
}

