
#include <config.h>

#include <gnome.h>
#include <libgnome/gnome-desktop-item.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-application-registry.h>

#include <stdlib.h>
#include <string.h>

#include "uf-view.h"
#include "uf-gui.h"

#undef DEBUG

#ifdef DEBUG
#define SAY(x...) g_message(x)
#else
#define SAY(x...)
#endif

uf_data *uf;
GtkWidget *uf_app;
GtkWidget *prefs = NULL;

int main(int argc, char *argv[])
{
	gchar *iconname;

	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	if (gnome_vfs_init () == FALSE)
	{
		g_warning ("Gaaaaaaaah! Gnome-VFS init failed mate !\n");
		exit(1);
	}

	gnome_program_init (PACKAGE, VERSION,
			LIBGNOMEUI_MODULE,
			argc, argv,
			GNOME_PARAM_APP_DATADIR, DATADIR,
			NULL);

	uf_app = create_uf_app();
	gtk_widget_show(uf_app);

	/* Set up the prefs */
	prefs = create_prefs(uf->selected_comic);
	g_object_add_weak_pointer (G_OBJECT (prefs),
			(void**)&(prefs));

	/* Icons */
	iconname = gnome_program_locate_file (NULL,
			GNOME_FILE_DOMAIN_APP_PIXMAP,
			"uf-view/gnome-uf-view.png", TRUE, NULL);
	if (iconname != NULL) {
		uf->icon = gdk_pixbuf_new_from_file(iconname, NULL);
		g_free(iconname);
	} else {
		uf->icon = NULL;
	}

	if (uf->icon != NULL)
		gtk_window_set_icon(GTK_WINDOW(uf_app), uf->icon);

	gtk_main();
	return 0;
}

static gint
xfer_progress_callback(GnomeVFSXferProgressInfo *info,
		gpointer data)
{
	while (gtk_events_pending ())
		gtk_main_iteration ();

	appbar_send_msg (_("Fetching %s"), (gchar *) data);
	SAY("%s", gnome_vfs_result_to_string (info->status));

	return TRUE;
}

void appbar_send_msg(const char *a, ...)
{
	gchar foo[2048];
	va_list ap;
	va_start(ap, a);

	vsnprintf(foo, 2000, a, ap);

	gnome_appbar_pop(uf->appbar);
	gnome_appbar_push(uf->appbar, foo);

	SAY("%s", foo);

	/* lets update the statusbar widget */
	while (gtk_events_pending())
		gtk_main_iteration();

	va_end(ap);
}

static void set_pixmap_to_be(char *url)
{
	GError *err = NULL;
	const char *mime_info;
	GnomeVFSMimeApplication *app;
	char *command, *msg;

	g_return_if_fail(url != NULL);
	SAY("%s", url);

	mime_info = gnome_vfs_get_mime_type (url);
	app = gnome_vfs_mime_get_default_application (mime_info);

	if (app != NULL)
	{
		command = g_strconcat (app->command, " \"", url, "\"", NULL);
		gnome_vfs_mime_application_free (app);
	} else {
		msg = g_strdup_printf (_("An error happened trying to display "
				"%s\nEither the The file doesn't exist, or you "
				"don't have a viewer for it."), url);
		show_error (msg);
		g_free (msg);
		return;
	}

	SAY("set_pixmap_to_be: url: %s", command);

	if (!g_spawn_command_line_async (command, &err))
	{
		msg = g_strdup_printf (_("Failed to open url: '%s'\n"
					"Details: %s"),
				url, err->message);
		show_error (msg);
		g_error_free (err);
		g_free (msg);
	}	
}

static void do_day_stuff(int *year, int *month, int *day, int days)
{
	struct tm foo = {
		0, 0, 12, *day, *month, *year - 1900
	};
	time_t f = mktime(&foo);

	f += (days * (60 * 60 * 24));
	localtime_r(&f, &foo);
	*year = foo.tm_year + 1900;
	*day = foo.tm_mday;
	*month = foo.tm_mon;
}

static void cause_movement(int way)
{
	gint year = uf->calendar->year;
	gint month = uf->calendar->month;
	gint day = uf->calendar->selected_day;

	do_day_stuff(&year, &month, &day, way);
	gtk_calendar_select_month(uf->calendar, month, year);
	gtk_calendar_select_day(uf->calendar, day);
}

void on_previous_click(GtkWidget *a, gpointer user_data)
{
	cause_movement(-1);
	on_fetch_button_clicked(a, user_data);
}

void on_next_click(GtkWidget *a, gpointer user_data)
{
	cause_movement(1);
	on_fetch_button_clicked(a, user_data);
}

void on_prefs_button_clicked(GtkWidget *a, gpointer user_data)
{
	gtk_widget_show(prefs);
	gdk_window_raise(prefs->window);
}

void on_comic_select_changed(GtkWidget *a, gpointer user_data)
{
	g_free (uf->selected_comic);

	uf->selected_comic = g_strdup (g_object_get_data (G_OBJECT (a),
				"filepath"));
	SAY("on_comic_select_changed: %s\n", uf->selected_comic);
}

void quit_app(GtkWidget * a, gpointer user_data)
{
	gnome_config_push_prefix ("/uf-view/General/");
	gnome_config_set_string ("selected_comic", uf->selected_comic);
	gnome_config_sync ();
	gnome_config_pop_prefix ();

	gtk_widget_destroy (uf_app);

	if (GTK_IS_WIDGET(prefs))
		gtk_widget_destroy (prefs);

	gtk_main_quit();
}

void about_app(GtkWidget * a, gpointer user_data)
{
	static GtkWidget *about = NULL;
	const gchar *authors[] = {
		("Bastien Nocera <hadess@hadess.net>"),
		("Robert Brady <rwb197@ecs.soton.ac.uk>"),
		NULL,
	};
	const gchar *translator_credits = _("translator_credits");

	if (about) {
		gdk_window_raise(about->window);
		return;
	}

	about = gnome_about_new(_("User Friendly Viewer"),
		VERSION,
		_("Copyright 1999-2002 Robert Brady, Bastien Nocera"),
		_("Calendar-based comics viewer"),
		(const gchar **) authors,
		NULL, /* documenters */
		strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
		uf->icon);

	g_signal_connect (G_OBJECT(about), "destroy",
			G_CALLBACK (gtk_widget_destroy), NULL);
	g_signal_connect (G_OBJECT (about), "delete-event",
			G_CALLBACK (gtk_widget_destroy), NULL);
	g_object_add_weak_pointer (G_OBJECT (about),
			(void**)&(about));
	gtk_widget_show (about);
}

void show_error(gchar *msg)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (GTK_WINDOW (uf_app),
			GTK_DIALOG_MODAL,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_CLOSE,
			msg, NULL);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

void on_today_button_clicked(GtkWidget * button, gpointer user_data)
{
	struct tm *time_struct;
	glong curtime;
	gint month, year, day;

	curtime = time (0);
	time_struct = localtime (&curtime);

	month = (time_struct->tm_mon); /* months start from 0 in GtkCalendar */
	year = (time_struct->tm_year)+1900;
	day = time_struct->tm_mday;

	gtk_calendar_freeze (uf->calendar);

	gtk_calendar_select_month (uf->calendar, month, year);
	gtk_calendar_select_day (uf->calendar, day);
	on_fetch_button_clicked (NULL, NULL);
	gtk_calendar_thaw (uf->calendar);
}

void on_fetch_button_clicked(GtkWidget * a, gpointer user_data)
{
	gtk_calendar_freeze (uf->calendar);
	uf_fetch (uf->calendar->year, uf->calendar->month+1,
			uf->calendar->selected_day);
	gtk_calendar_thaw (uf->calendar);
}

gboolean date_error(int year, int month, int day,
		int debut_year, int debut_month, int debut_day)
{
	if (year < debut_year)
		return TRUE;

	if (year == debut_year && month < debut_month)
		return TRUE;

	if (year == debut_year && month == debut_month && day < debut_day)
		return TRUE;

	return FALSE;
}

GnomeVFSFileSize get_size(GnomeVFSURI *uri)
{
	GnomeVFSFileInfoOptions info_options;
	GnomeVFSFileInfo *info;
	GnomeVFSFileSize len;
	GnomeVFSResult result;

	info_options = GNOME_VFS_FILE_INFO_DEFAULT;

	info = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info_uri(uri, info, info_options);
	
	if (result != GNOME_VFS_OK)
		return 0;

	len = (unsigned long long)info->size;
	gnome_vfs_file_info_unref(info);

	return len;
}

GnomeVFSURI *xfer_webpage(gchar *src, gchar *dest)
{
	GnomeVFSURI *src_uri, *dest_uri;
	GnomeVFSResult result;
	GnomeVFSXferOptions xfer_options;

	xfer_options = GNOME_VFS_XFER_DEFAULT;
	
	src_uri = gnome_vfs_uri_new(src);
	dest_uri = gnome_vfs_uri_new(dest);

	result = gnome_vfs_xfer_uri(src_uri, dest_uri,
			xfer_options, GNOME_VFS_XFER_ERROR_MODE_QUERY,
			GNOME_VFS_XFER_OVERWRITE_MODE_REPLACE,
			xfer_progress_callback, src);
	gnome_vfs_uri_unref(src_uri);

	if (result != GNOME_VFS_OK)
		return NULL;

	return dest_uri;
}

void uf_fetch (int year, int month, int day)
{
	GnomeDesktopItem *ditem;
	const char *name;
	int short_year;

	if (uf->selected_comic == NULL)
	{
		show_error (_("Select a comic from the preferences first"));
		return;
	}

	ditem = gnome_desktop_item_new_from_file (uf->selected_comic, 0, NULL);

	if (ditem == NULL)
	{
		show_error (_("Select a comic from the preferences first"));
		return;
	}

	/* check type */
	{
		int type;

		type = gnome_desktop_item_get_entry_type(ditem);
		if (type != GNOME_DESKTOP_ITEM_TYPE_LINK)
		{
			gnome_desktop_item_unref (ditem);
			show_error (_("Select a comic from the preferences first"));
		}
	}

	/* Won't fail, it's needed for the desktop file to be proper */
	name = gnome_desktop_item_get_string (ditem, GNOME_DESKTOP_ITEM_NAME);

	/* check start date
	 * ATTENTION: this is checked before the year offset */
	{
		const char *date;
		int start_year, start_month, start_day;

		date = gnome_desktop_item_get_string (ditem,
				"X-UFVIEW-StartDate");
		if (date == NULL)
		{
			gnome_desktop_item_unref (ditem);
			show_error (_("Select a comic from the preferences first"));
			return;
		}

		sscanf (date, "%d-%d-%d",
				&start_year, &start_month, &start_day);
		if (date_error(year, month, day,
				start_year, start_month, start_day) == TRUE)
		{
			char *msg;

			msg = g_strdup_printf
				(_("%s did not exist at that time.\n"
				   "Please choose a later date."), name);
			show_error(msg);
			g_free (msg);

			gnome_desktop_item_unref (ditem);
			return;
		}
	}

	/* Seems we're gone for it, we need some more info now */
	{
		const char *str_offset;
		int offset = 0;

		str_offset = gnome_desktop_item_get_string (ditem,
				"X-UFVIEW-YearOffset");
		if (str_offset != NULL)
		{
			offset = atoi (str_offset);
		}
		year = year + offset;
		short_year = year%100;
	}

	/* easy case, direct to url */
	{
		const char *format;
		char *url;

		format = gnome_desktop_item_get_string (ditem,
				"X-UFVIEW-Url");
		if (format != NULL)
		{
			url = g_strdup_printf (format,
					year, short_year, month, day);

			set_pixmap_to_be(url);
			appbar_send_msg(_("%s for %04i-%02i-%02i"),
					name, year, month, day);
			gnome_desktop_item_unref (ditem);
			return;
		}
	}

	{
		const char *url_format;
		char *url, *body;
		char tmpfile[100] = "/tmp/ufXXXXXX";

		url_format = gnome_desktop_item_get_string (ditem,
				"X-UFVIEW-UrlSearch");
		url = g_strdup_printf (url_format,
				year, short_year, month, day);

		appbar_send_msg(_("Fetching %s"), url);

		mkstemp(tmpfile);
		if (xfer_webpage(url, tmpfile) == FALSE)
		{
			show_error(_("Couldn't contact the website\n"
					"Check your internet settings"));
			g_free (url);
			gnome_desktop_item_unref (ditem);
			unlink (tmpfile);
			g_free (tmpfile);
			return;
		}

		g_free (url);
		if (g_file_get_contents (tmpfile, &body, NULL, NULL) == FALSE)
		{
			show_error(_("Couldn't contact the website\n"
					"Check your internet settings"));
			gnome_desktop_item_unref (ditem);
			unlink (tmpfile);
			g_free (tmpfile);
			return;
		}

		unlink (tmpfile);
		{
			int fish_offset, fish_length;
			char *fish;
			char *imgname = strstr (body,
				gnome_desktop_item_get_string (ditem,
					"X-UFVIEW-StringSearch"));

			if ((imgname == NULL) && gnome_desktop_item_get_string
					(ditem, "X-UFVIEW-StringSearch2"))
				imgname = strstr (body,
					gnome_desktop_item_get_string (ditem,
						"X-UFVIEW-StringSearch2"));

			g_free (body);

			if (imgname == NULL)
			{
				show_error (_("Couldn't figure out where "
						"the comic was. It's probably "
						"moved again."));
				gnome_desktop_item_unref (ditem);
				return;
			}

			fish_offset = atoi (gnome_desktop_item_get_string
					(ditem, "X-UFVIEW-FishOffset"));
			fish_length = atoi (gnome_desktop_item_get_string
					(ditem, "X-UFVIEW-FishLength"));

			fish = g_strndup (imgname + fish_offset,
					fish_length);

			//FIXME what about if we need a fish_format
			set_pixmap_to_be (fish);
			appbar_send_msg(_("%s for %04i-%02i-%02i"),
					name, year, month, day);

			g_free (fish);
		}
	}

	gnome_desktop_item_unref (ditem);
	return;
}

