#include <gnome.h>
#include <libgnomevfs/gnome-vfs.h>

void show_error(gchar *msg);
void appbar_send_msg(const char *a, ...);
gboolean date_error(int year, int month, int day,
	int debut_year, int debut_month, int debut_day);
GnomeVFSFileSize get_size(GnomeVFSURI *uri);
GnomeVFSURI *xfer_image(gchar *src, gchar *dest);
gchar *copy_to_mem(GnomeVFSURI *uri, GnomeVFSFileSize len);

/* Fetcher for the comics */
void uf_fetch(int year, int month, int day);
void mg_fetch(int year, int month, int day);
void pa_fetch(int year, int month, int day);
void ch_fetch(int year, int month, int day);
void lg_fetch(int year, int month, int day);
void at_fetch(int year, int month, int day);
void w9_fetch(int year, int month, int day);

void on_fetch_button_clicked (GtkWidget *button, gpointer user_data);
void on_today_button_clicked (GtkWidget *button, gpointer user_data);
void on_next_click (GtkWidget*, gpointer user_data);
void on_previous_click (GtkWidget*, gpointer user_data);
void on_prefs_button_clicked(GtkWidget*, gpointer user_data);
void on_comic_select_changed(GtkWidget * a, gpointer user_data);
void quit_app (GtkWidget*, gpointer user_data);
void about_app (GtkWidget*, gpointer user_data);

typedef struct {
  GtkCalendar *calendar;
  GtkImage *pixmap;
  GnomeAppBar *appbar;
  gchar *selected_comic;
  GdkPixbuf *icon;
} uf_data;

extern uf_data *uf;
